local component_code = "local parentId=" .. os.getComputerID() .. [[

local my_inventory = peripheral.find("inventory")
local data = { name = os.getComputerLabel(), protocol = "farm_manager", inventory_size = my_inventory.size() * my_inventory.getItemLimit(1) }

function inventoryCount()
    local count = 0
    for key, item in pairs(my_inventory.list()) do
        count = count + item.count
    end
    return count
end
if type(peripheral.getType(my_inventory))=="string" then
    if string.find((peripheral.getType(my_inventory)),"drawer") then
        data.inventory_size = (my_inventory.size()-1)*my_inventory.getItemLimit(1)
    end
else
    for index, type in ipairs(peripheral.getType(my_inventory)) do
        if string.find(type,"drawer") then
            data.inventory_size = (my_inventory.size()-1)*my_inventory.getItemLimit(1)
        end
    end
end

print(my_inventory.getItemLimit(1))
data.count = inventoryCount()
data.inventory_size_percent = data.count * 100 / data.inventory_size
rednet.send(parentId, data)]]

local protocol = "farm_manager"
local ACTIVE_MAX = 10 -- number of calls to set a inventory as inactive
local current_inventories = {}
local monitor = peripheral.find("monitor")
monitor.setTextScale(0.5)
monitor.setBackgroundColor(colors.black)
rednet.open("right")
rednet.open("left")
local function display(inv)
    local name = inv.name
    local invData = { inv.count, inv.inventory_size, inv.inventory_size_percent, 0 }
    if current_inventories[name] and invData[1] == current_inventories[name][1] then
        current_inventories[name][4] = current_inventories[name][4] + 1
    else
        current_inventories[name] = invData
    end
end

local function paint()
    monitor.setCursorPos(1, 1)
    monitor.write("LINKED FARMS")
    monitor.setCursorPos(1, 3)
    monitor.setBackgroundColor(colors.lightGray)
    monitor.write("                                                              ")
    monitor.setBackgroundColor(colors.black)
    local valueY = 5
    for key, invData in pairs(current_inventories) do
        monitor.setCursorPos(1, valueY)
        monitor.write(key .. " -> " .. tostring(invData[1]) .. "/" .. tostring(invData[2]) .. " - ")
        if invData[3] < 25.0 then
            monitor.setTextColor(colors.green)
        elseif invData[3] > 25.0 and invData[3] < 75.0 then
            monitor.setTextColor(colors.yellow)
        else
            monitor.setTextColor(colors.red)
        end
        monitor.write(string.format("%.2f", invData[3]) .. "%")
        monitor.setTextColor(colors.white)
        if invData[4] < ACTIVE_MAX then
            monitor.setBackgroundColor(colors.green)
        else
            monitor.setBackgroundColor(colors.red)
        end
        monitor.setCursorPos(55,valueY)
        monitor.write(" ")
        monitor.setBackgroundColor(colors.black)
        valueY = valueY + 2
    end
    monitor.setCursorPos(1, valueY + 1)
    monitor.setBackgroundColor(colors.lightGray)
    monitor.write("                                                              ")
    monitor.setBackgroundColor(colors.black)
end

while true do
    local components = rednet.lookup("inventory_status_component")
    if not components then
        print("No inventory_status_component found at the network.")
    elseif type(components) == "number" then
        rednet.send(components, component_code)
        local id, message = rednet.receive(nil, 5)
        if message and protocol == message.protocol then
            display(message)
            monitor.clear()
            paint()
        end
    else
        for index, id in ipairs(components) do
            rednet.send(id, component_code)
            local id, message
            rednet.receive(nil, 5)
            if message and protocol == message.protocol then
                display(message)
                monitor.clear()
                paint()
            end
        end
    end
    sleep(0.1)
end
