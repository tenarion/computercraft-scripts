if not os.getComputerLabel() then
    os.setComputerLabel("Inventory Status Component")
end
peripheral.find("modem", rednet.open)
rednet.host("inventory_status_component", os.getComputerLabel())
while true do
    local msg
    repeat
         local id, message = rednet.receive()
         msg = message
    until type(message) == "string"
    print("Sending data to parent...")
    local fn, err = load(msg)
    if fn then
         fn()
    else
         print("Error loading code " .. err)
    end
end
