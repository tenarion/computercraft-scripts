local component = http.get(
"https://gitlab.com/tenarion/computercraft-scripts/-/raw/main/linked-farm-manager-lite/inventoryStatusComponent.lua")
local fn, err = load(component.readAll())
if fn then
     fn()
else
     print("Error loading code " .. err)
end
