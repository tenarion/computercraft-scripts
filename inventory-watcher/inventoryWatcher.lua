function execute(inventory, monitor)
    local chest = peripheral.wrap(inventory)
    local monitor = peripheral.wrap(monitor)
    monitor.setTextScale(0.5)
    monitor.setCursorPos(1, 1)
    monitor.setBackgroundColor(colors.black)
    function getCurrentItemSize()
        local totalItems = 0
        for slot, item in pairs(chest.list()) do
            totalItems = totalItems + item.count
        end
        return totalItems
    end

    local scroll = 0
    local maxBarNum = 26
    local monitorX, monitorY = monitor.getSize()
    while true do
        local maxItems = (chest.size() - 1) * chest.getItemLimit(1)
        monitor.setBackgroundColor(colors.black)
        monitor.clear()
        local currentItems = getCurrentItemSize()
        local currentBarNum = (currentItems / maxItems) * maxBarNum
        local percent = (currentItems * 100 / maxItems)
        monitor.setCursorPos((monitorX / 2) - 14, 8)
        monitor.write(("%d / %d items almacenados"):format(currentItems, maxItems))
        monitor.setCursorPos((monitorX) / 2 - 5, 14)
        monitor.write(("%.2f/100.00%%"):format(percent))
        for i = 1, maxBarNum do
            local currentColor = colors.white
            if percent <= 50 then
                currentColor = colors.green
            elseif percent > 50 and percent <= 75 then
                currentColor = colors.orange
            else
                currentColor = colors.red
            end
            if i <= currentBarNum then
                monitor.setCursorPos(monitorX / 2 + i - (maxBarNum / 2), monitorY / 2 - 1)
                monitor.write(" ")
                monitor.setCursorPos(monitorX / 2 + i - (maxBarNum / 2), monitorY / 2)
                monitor.write(" ")
                monitor.setBackgroundColor(currentColor)
            else
                monitor.setCursorPos(monitorX / 2 + i - (maxBarNum / 2), monitorY / 2 - 1)
                monitor.write(" ")
                monitor.setCursorPos(monitorX / 2 + i - (maxBarNum / 2), monitorY / 2)
                monitor.write(" ")
                monitor.setBackgroundColor(colors.gray)
            end
        end
        monitor.setBackgroundColor(colors.white)
        monitor.setCursorPos(1, 16)
        monitor.write("                                                                 ")
        monitor.setBackgroundColor(colors.black)
        for i = 1, chest.size() do
            local item = chest.getItemDetail(i)
            if item then
                local cursorX, cursorY = monitor.getCursorPos()
                local itemName = string.sub(item.name, string.find(item.name, ":") + 1)
                local firstItemName = string.sub(itemName, 0, string.find(itemName, "_") - 1)
                local secondItemName = string.sub(itemName, string.find(itemName, "_") + 1)
                monitor.setCursorPos(1, cursorY + 2)
                monitor.write(("%d/%d - %s"):format(item.count, maxItems / (chest.size() - 1),
                    string.upper(string.sub(firstItemName, 1, 1)) ..
                    string.sub(firstItemName, 2) ..
                    " " .. string.upper(string.sub(secondItemName, 1, 1)) .. string.sub(secondItemName, 2)))
            end
        end
        sleep(10)
    end
end
